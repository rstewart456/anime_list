# The Back end to the anime list.
The Backend uses Graphql-yoga. Graphql-yoga was one of the mostly easier to use Graphql server with typescript.

## Technology
- Graphql-yoga
- Typescript
- Mongoose

# To Do
- [X] Connect to the DataBase
- [X] Create the List
- [X] Create the user
- [X] Generate the Json Web Token
- [X] Verify the Token
- [X] Store Tokens in the DataBase.
- [X] Check to see if the token is in the Database.
- [ ] Error Messages

## Future request
- [ ] Create a Federation to Anilist
- [ ] Federation to get Charactors of the anime.
