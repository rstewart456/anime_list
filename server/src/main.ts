import { createServer } from '@graphql-yoga/node';
import { schema } from './schema';
import { config } from 'dotenv'
import { connectDB } from './config/db';

config()

async function main() {
  await connectDB(process.env.MONGO_URL as string)
  const server = createServer({ schema })
  await server.start()
}

main()
