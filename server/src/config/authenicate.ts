import { config } from 'dotenv';
import jwt from 'jsonwebtoken';

config()

export const accessToken = (user: string) => {
  return jwt.sign({id: user}, process.env.JWT_KEY!, { expiresIn: '30d' })
}

interface Jwt {
  id: string
}

export function verifyToken(token: string) {
  try {
    const bob = jwt.verify(token, process.env.JWT_KEY!) as Jwt
    return bob
  } catch(e) {
    return {id: "error"}
  }
}
