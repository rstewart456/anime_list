import axios from 'axios';
import { NextSeason } from '../models/nextseason'


// Functions to figure out the next season
function nextSeason() {
    let WhatSeason: string = ''
    let SeasonYear: number
    let CurrentSeason: string = ''
    // It's plus one because January is index 0
    const now = new Date();
    const month = now.getMonth() + 1;

    if (month > 3 && month < 6) {
        CurrentSeason = "spring";
    }

    if (month > 6 && month < 9) {
        CurrentSeason = "summer";
    }

    if (month > 9 && month < 12) {
        CurrentSeason = "fall";
    }

    if (month >= 1 && month < 3) {
        CurrentSeason = "winter";
    }

    const day = now.getDate();
    if (month === 3) {
        if (day < 22) {
            CurrentSeason = "winter";
        } else {
            CurrentSeason = "spring";
        }
    }

    if (month === 6) {
        if (day < 22) {
            CurrentSeason = "spring";
        } else {
            CurrentSeason = "summer";
        }
    }

    if (month === 9) {
        if (day < 22) {
            CurrentSeason = "summer";
         } else {
            CurrentSeason = "fall";
        }
    }

    if (month === 12) {
        if (day < 22) {
            CurrentSeason = "fall";
        } else {
            CurrentSeason = "winter";
        }
    }

    switch (CurrentSeason) {
        case "winter":
            WhatSeason = "SPRING";
            break;
        case "spring":
            WhatSeason = "SUMMER";
            break;
        case "summer":
            WhatSeason = "FALL";
            break;
        case "fall":
            WhatSeason = "WINTER";
    }
    const year = new Date();
    if (WhatSeason === "WINTER") {
        SeasonYear = year.getFullYear() + 1;
    } else {
        SeasonYear = year.getFullYear();
    }
    return {
        WhatSeason,
        SeasonYear
    }
    //console.error("Unable to calculate current season");
}

export const typesDef = `
  type Query {
    showseason(page: Int!, limit: Int!): Pagination
  }


  type Pagination {
     totalPages: Int
     page: Int
     hasMore: Boolean
     limit: Int
     docs: [Anime]
  }

  type Anime {
      id: ID
      anilistId: Int!
      title: String!
      image: String!
      description: String
      genres: [String]
      release: Int
      episodes: Int
      ErrorMessage: String
  }

  type Mutation {
    getseason: [Anime]
  }
`

export const resolver = {
    Query: {
      showseason: async(_: unknown, args: any) => {
          const options = {
              page: args.page,
              limit: 6
          }
          const mother = await NextSeason.paginate(options, function(err: Error, results: any) {
              if (err) {
                  return err
              } else {
                  return results
              }})
          return (mother)
      }
    },
    Mutation: {
        getseason: async (_: unknown, ) => {
            if (nextSeason().WhatSeason == 'WINTER') {
                const endpoint = "https://graphql.anilist.co";
                const headers = {
                    "content-type": "application/json",
                };
                const graphqlQuery = {
                    query: `
              query ($seasonYear: Int!, $perPage: Int!) {
                Page (perPage: $perPage) {
                media(id: $id, season: WINTER, seasonYear: $seasonYear type: ANIME isAdult: false) {
                  id
                  title {
                    romaji
                    english
                  }
                  coverImage {
                  extraLarge
                  }
                  description
                  seasonYear
                  episodes
                  genres
                }
              }
            }
            `,
                    variables: {
                        season: nextSeason().WhatSeason,
                        seasonYear: nextSeason().SeasonYear,
                        perPage: 100,
                    },
                };
                const response = await axios({
                    url: endpoint,
                    method: "POST",
                    headers: headers,
                    data: graphqlQuery,
                });
                let animeArray = []

                const anime = response.data.data.Page.media
                // For Loop to convert Anilist Naming to My Naming System.
                for (let i = 0; i < anime.length; ++i) {

                    let titleName = ''

                    if (anime[i].title.english == undefined) {
                        titleName = anime[i].title.romaji
                    } else {
                        titleName = anime[i].title.english
                    }

                    const ani = {
                        title: titleName,
                        anilistId: anime[i].id,
                        image: anime[i].coverImage.extraLarge,
                        description: anime[i].description,
                        release: anime[i].seasonYear,
                        episodes: anime[i].episodes,
                        genres: anime[i].genres
                    }
                    animeArray.push(ani)
                }

                await NextSeason.collection.drop();
                await NextSeason.insertMany(animeArray)
                return (animeArray)

            } else if (nextSeason().WhatSeason == 'SPRING') {
                const endpoint = "https://graphql.anilist.co";
                const headers = {
                    "content-type": "application/json",
                };
                const graphqlQuery = {
                    query: `
              query ($seasonYear: Int!, $perPage: Int!) {
                Page (perPage: $perPage) {
                media(season: SPRING, seasonYear: $seasonYear type: ANIME isAdult: false) {
                  id
                  title {
                    romaji
                    english
                  }
                  coverImage {
                  extraLarge
                  }
                  description
                  seasonYear
                  episodes
                  genres
                }
              }
            }
            `,
                    variables: {
                        season: nextSeason().WhatSeason,
                        seasonYear: nextSeason().SeasonYear,
                        perPage: 100,
                    },
                };
                const response = await axios({
                    url: endpoint,
                    method: "POST",
                    headers: headers,
                    data: graphqlQuery,
                });
                let animeArray = []

                const anime = response.data.data.Page.media
                // For Loop to convert Anilist Naming to My Naming System.
                for (let i = 0; i < anime.length; ++i) {

                    let titleName = ''

                    if (anime[i].title.english == undefined) {
                        titleName = anime[i].title.romaji
                    } else {
                        titleName = anime[i].title.english
                    }

                    const ani = {
                        title: titleName,
                        anilistId: anime[i].id,
                        image: anime[i].coverImage.extraLarge,
                        description: anime[i].description,
                        release: anime[i].seasonYear,
                        episodes: anime[i].episodes,
                        genres: anime[i].genres
                    }
                    animeArray.push(ani)
                }

                await NextSeason.collection.drop();
                await NextSeason.insertMany(animeArray)
                return (animeArray)

            } else if (nextSeason().WhatSeason == 'SUMMER') {
                const endpoint = "https://graphql.anilist.co";
                const headers = {
                    "content-type": "application/json",
                };
                const graphqlQuery = {
                    query: `
              query ($id: Int, $seasonYear: Int!, $perPage: Int!) {
                Page (perPage: $perPage) {
                media(id: $id, season: SUMMER, seasonYear: $seasonYear type: ANIME isAdult: false) {
                  id
                  title {
                    romaji
                    english
                  }
                  coverImage {
                  extraLarge
                  }
                  description
                  seasonYear
                  episodes
                  genres
                }
              }
            }
            `,
                    variables: {
                        season: nextSeason().WhatSeason,
                        seasonYear: nextSeason().SeasonYear,
                        perPage: 100,
                    },
                };
                const response = await axios({
                    url: endpoint,
                    method: "POST",
                    headers: headers,
                    data: graphqlQuery,
                });
                let animeArray = []

                const anime = response.data.data.Page.media
                // For Loop to convert Anilist Naming to My Naming System.
                for (let i = 0; i < anime.length; ++i) {

                    let titleName = ''

                    if (anime[i].title.english == undefined) {
                        titleName = anime[i].title.romaji
                    } else {
                        titleName = anime[i].title.english
                    }

                    const ani = {
                        title: titleName,
                        anilistId: anime[i].id,
                        image: anime[i].coverImage.extraLarge,
                        description: anime[i].description,
                        release: anime[i].seasonYear,
                        episodes: anime[i].episodes,
                        genres: anime[i].genres
                    }
                    animeArray.push(ani)
                }

                await NextSeason.collection.drop();
                await NextSeason.insertMany(animeArray)

                return (animeArray)

            } else {
                const endpoint = "https://graphql.anilist.co";
                const headers = {
                    "content-type": "application/json",
                };
                const graphqlQuery = {
                    query: `
              query ($seasonYear: Int!, $perPage: Int!) {
                Page (perPage: $perPage) {
                media(season: FALL, seasonYear: $seasonYear type: ANIME isAdult: false) {
                  id
                  title {
                    romaji
                    english
                  }
                  coverImage {
                  extraLarge
                  }
                  description
                  seasonYear
                  episodes
                  genres
                }
              }
            }
            `,
                    variables: {
                        season: nextSeason().WhatSeason,
                        seasonYear: nextSeason().SeasonYear,
                        perPage: 100,
                    },
                };
                const response = await axios({
                    url: endpoint,
                    method: "POST",
                    headers: headers,
                    data: graphqlQuery,
                });
                let animeArray = []

                const anime = response.data.data.Page.media
                // For Loop to convert Anilist Naming to My Naming System.
                for (let i = 0; i < anime.length; ++i) {

                    let titleName = ''

                    if (anime[i].title.english == undefined) {
                        titleName = anime[i].title.romaji
                    } else {
                        titleName = anime[i].title.english
                    }

                    const ani = {
                        title: titleName,
                        anilistId: anime[i].id,
                        image: anime[i].coverImage.extraLarge,
                        description: anime[i].description,
                        release: anime[i].seasonYear,
                        episodes: anime[i].episodes,
                        genres: anime[i].genres
                    }
                    animeArray.push(ani)
                }
                await NextSeason.collection.drop();
                await NextSeason.insertMany(animeArray)

                return (animeArray)
            }
        },
    },

}
