import Anime from '../models/anime'
import User from '../models/user'
import { verifyToken } from '../config/authenicate'
import { Animes } from '../types'

export const typesDef = `
    type Query {
        anime(anilistId: Int!): Anime
        animes: [Anime]
    }

    type Anime {
        id: ID!
        anilistId: Int!
        title: String!
        image: String!
        description: String
        genres: [String]
        release: Int
        episodes: Int
        token: String!
        ErrorMessage: String

    }

    type Mutation {
        addAnime(anilistId: Int!, title: String!, image: String!, description: String!, genres: [String], release: Int, episodes: Int, token: String!): Anime,
    }
`

export const resolver = {
    Query: {
        animes: async () => {
            const animes = await Anime.find();
            return animes;
        },
        anime: async (_: unknown, args: any) => {
            const anime = await Anime.findOne({ anilistId: args.anilistId });
            return anime;
        },
    },
    Mutation: {
        addAnime: async (_: unknown, args: Animes) => {
            try {
                const token = verifyToken(args.token)
                let checkToken: boolean = false
                // Get User Information
                const users = await User.findOne({ _id: token.id })
                // Checks to see if the user Tokens
                if (users?.tokens) {
                  // Loops through the Tokens
                  for (let x = 0; x < users.tokens.length; x++) {
                    // Check To see if the user token is in the token list
                    if (users.tokens[x].token === args.token) {
                      // If Token is Found. If changes CheckToken to True
                      checkToken = true
                    }
                  }
                }
                if (checkToken) {
                    const anime = await Anime.findOne({ anilistId: args.anilistId });
                    if (anime) {
                        anime.anilistId = args.anilistId
                        anime.title = args.title
                        anime.image = args.image
                        anime.description = args.description
                        anime.genres = args.genres
                        anime.release = args.release
                        anime.episodes = args.episodes
                        await anime.save()
                        const updateAnime = await Anime.findOne({ anilistId: args.anilistId })
                        return updateAnime
                    } else {
                        let response = await Anime.create({
                            anilistId: args.anilistId,
                            title: args.title,
                            image: args.image,
                            description: args.description,
                            genres: args.genres,
                            release: args.release,
                            episodes: args.episodes
                        });
                        return response;
                    }
                } else {
                    new Error("Please Log Back In")
                }
            } catch (e) {
                console.log(e);
            }
        },
    }
}
