import axios from 'axios';
import { Search } from '../types'

export const typesDef = `
    type Query {
        searches(search: String!, page: Int, perPage: Int): Search
    }

    type Search {
        id: ID!
        total: Int
        currentPage: Int
        lastPage: Int
        hasNextPage: Boolean
        perPage: Int
        search: String!
        animes: [Anime]
    }
`

export const resolver = {
    Query: {
        searches: async ( _: unknown, args: Search) => {
            const endpoint = "https://graphql.anilist.co";
            const headers = {
                "content-type": "application/json",
            };
            const graphqlQuery = {
                query: `
              query ($id: Int, $page: Int, $perPage: Int, $search: String!) {
                Page (page: $page, perPage: $perPage) {
                  pageInfo {
                  total
                  currentPage
                  lastPage
                  hasNextPage
                  perPage
                  }
                media(id: $id, search: $search, type: ANIME) {
                  id
                  title {
                    romaji
                    english
                  }
                  coverImage {
                  extraLarge
                  }
                  description
                  seasonYear
                  episodes
                  genres
                }
              }
              }
            `,
                variables: {
                    search: args.search,
                    page: args.page,
                    perPage: args.perPage,
                },
            };
            const response = await axios({
                url: endpoint,
                method: "POST",
                headers: headers,
                data: graphqlQuery,
            });
            let animeArray = []

            const anime = response.data.data.Page.media
            // For Loop to convert Anilist Naming to My Naming System.
            for (let i = 0; i < anime.length; ++i) {

                let titleName = ''

                if (anime[i].title.english == undefined) {
                    titleName = anime[i].title.romaji
                } else {
                    titleName = anime[i].title.english
                }

                const ani = {
                    id: anime[i].id,
                    title: titleName,
                    anilistId: anime[i].id,
                    image: anime[i].coverImage.extraLarge,
                    description: anime[i].description,
                    release: anime[i].seasonYear,
                    episodes: anime[i].episodes,
                    genres: anime[i].genres
                }
                animeArray.push(ani)
            }

            const fetchAnime = {
                total: response.data.data.Page.pageInfo.total,
                currentPage: response.data.data.Page.pageInfo.currentPage,
                lastPage: response.data.data.Page.pageInfo.lastPage,
                hasNextPage: response.data.data.Page.pageInfo.hasNextPage,
                perPage: response.data.data.Page.pageInfo.perPage,
                animes: animeArray
            };

            return fetchAnime;
        },
    },

}
