import axios from 'axios';
import { Season } from '../types'

export const typesDef = `
    type Query {
        season(season: String seasonYear: Int page: Int perPage: Int): Season
    }

    type Season {
        id: ID!
        total: Int
        currentPage: Int
        lastPage: Int
        hasNextPage: Boolean
        perPage: Int
        season: String
        seasonYear: Int!
        animes: [Anime]
    }
`

export const resolver = {
    Query: {
        season: async (_: unknown, args: Season) => {
            if (args.season == 'WINTER') {
                const endpoint = "https://graphql.anilist.co";
                const headers = {
                    "content-type": "application/json",
                };
                const graphqlQuery = {
                    query: `
              query ($id: Int, $page: Int, $perPage: Int, $seasonYear: Int!) {
                Page (page: $page, perPage: $perPage) {
                  pageInfo {
                  total
                  currentPage
                  lastPage
                  hasNextPage
                  perPage
                  }
                media(id: $id, season: WINTER, seasonYear: $seasonYear type: ANIME isAdult: false) {
                  id
                  title {
                    romaji
                    english
                  }
                  coverImage {
                  extraLarge
                  }
                  description
                  seasonYear
                  episodes
                  genres
                }
              }
              }
            `,
                    variables: {
                        season: args.season,
                        seasonYear: args.seasonYear,
                        page: args.page,
                        perPage: args.perPage,
                    },
                };
                const response = await axios({
                    url: endpoint,
                    method: "POST",
                    headers: headers,
                    data: graphqlQuery,
                });
                let animeArray = []

                const anime = response.data.data.Page.media
                // For Loop to convert Anilist Naming to My Naming System.
                for (let i = 0; i < anime.length; ++i) {

                    let titleName = ''

                    if (anime[i].title.english == undefined) {
                        titleName = anime[i].title.romaji
                    } else {
                        titleName = anime[i].title.english
                    }

                    const ani = {
                        id: anime[i].id,
                        title: titleName,
                        anilistId: anime[i].id,
                        image: anime[i].coverImage.extraLarge,
                        description: anime[i].description,
                        release: anime[i].seasonYear,
                        episodes: anime[i].episodes,
                        genres: anime[i].genres
                    }
                    animeArray.push(ani)
                }

                const fetchAnime = {
                    total: response.data.data.Page.pageInfo.total,
                    currentPage: response.data.data.Page.pageInfo.currentPage,
                    lastPage: response.data.data.Page.pageInfo.lastPage,
                    hasNextPage: response.data.data.Page.pageInfo.hasNextPage,
                    perPage: response.data.data.Page.pageInfo.perPage,
                    animes: animeArray
                };

                return fetchAnime;
            } else if(args.season == 'SPRING') {
                const endpoint = "https://graphql.anilist.co";
                const headers = {
                    "content-type": "application/json",
                };
                const graphqlQuery = {
                    query: `
              query ($id: Int, $page: Int, $perPage: Int, $seasonYear: Int!) {
                Page (page: $page, perPage: $perPage) {
                  pageInfo {
                  total
                  currentPage
                  lastPage
                  hasNextPage
                  perPage
                  }
                media(id: $id, season: SPRING, seasonYear: $seasonYear type: ANIME isAdult: false) {
                  id
                  title {
                    romaji
                    english
                  }
                  coverImage {
                  extraLarge
                  }
                  description
                  seasonYear
                  episodes
                  genres
                }
              }
              }
            `,
                    variables: {
                        season: args.season,
                        seasonYear: args.seasonYear,
                        page: args.page,
                        perPage: args.perPage,
                    },
                };
                const response = await axios({
                    url: endpoint,
                    method: "POST",
                    headers: headers,
                    data: graphqlQuery,
                });
                let animeArray = []

                const anime = response.data.data.Page.media
                // For Loop to convert Anilist Naming to My Naming System.
                for (let i = 0; i < anime.length; ++i) {

                    let titleName = ''

                    if (anime[i].title.english == undefined) {
                        titleName = anime[i].title.romaji
                    } else {
                        titleName = anime[i].title.english
                    }

                    const ani = {
                        id: anime[i].id,
                        title: titleName,
                        anilistId: anime[i].id,
                        image: anime[i].coverImage.extraLarge,
                        description: anime[i].description,
                        release: anime[i].seasonYear,
                        episodes: anime[i].episodes,
                        genres: anime[i].genres
                    }
                    animeArray.push(ani)
                }

                const fetchAnime = {
                    total: response.data.data.Page.pageInfo.total,
                    currentPage: response.data.data.Page.pageInfo.currentPage,
                    lastPage: response.data.data.Page.pageInfo.lastPage,
                    hasNextPage: response.data.data.Page.pageInfo.hasNextPage,
                    perPage: response.data.data.Page.pageInfo.perPage,
                    animes: animeArray
                };

                return fetchAnime;
            } else if(args.season == 'SUMMER') {
                const endpoint = "https://graphql.anilist.co";
                const headers = {
                    "content-type": "application/json",
                };
                const graphqlQuery = {
                    query: `
              query ($id: Int, $page: Int, $perPage: Int, $seasonYear: Int!) {
                Page (page: $page, perPage: $perPage) {
                  pageInfo {
                  total
                  currentPage
                  lastPage
                  hasNextPage
                  perPage
                  }
                media(id: $id, season: SUMMER, seasonYear: $seasonYear type: ANIME isAdult: false) {
                  id
                  title {
                    romaji
                    english
                  }
                  coverImage {
                  extraLarge
                  }
                  description
                  seasonYear
                  episodes
                  genres
                }
              }
              }
            `,
                    variables: {
                        season: args.season,
                        seasonYear: args.seasonYear,
                        page: args.page,
                        perPage: args.perPage,
                    },
                };
                const response = await axios({
                    url: endpoint,
                    method: "POST",
                    headers: headers,
                    data: graphqlQuery,
                });
                let animeArray = []

                const anime = response.data.data.Page.media
                // For Loop to convert Anilist Naming to My Naming System.
                for (let i = 0; i < anime.length; ++i) {

                    let titleName = ''

                    if (anime[i].title.english == undefined) {
                        titleName = anime[i].title.romaji
                    } else {
                        titleName = anime[i].title.english
                    }

                    const ani = {
                        id: anime[i].id,
                        title: titleName,
                        anilistId: anime[i].id,
                        image: anime[i].coverImage.extraLarge,
                        description: anime[i].description,
                        release: anime[i].seasonYear,
                        episodes: anime[i].episodes,
                        genres: anime[i].genres
                    }
                    animeArray.push(ani)
                }

                const fetchAnime = {
                    total: response.data.data.Page.pageInfo.total,
                    currentPage: response.data.data.Page.pageInfo.currentPage,
                    lastPage: response.data.data.Page.pageInfo.lastPage,
                    hasNextPage: response.data.data.Page.pageInfo.hasNextPage,
                    perPage: response.data.data.Page.pageInfo.perPage,
                    animes: animeArray
                };

                return fetchAnime;
            } else {
                const endpoint = "https://graphql.anilist.co";
                const headers = {
                    "content-type": "application/json",
                };
                const graphqlQuery = {
                    query: `
              query ($id: Int, $page: Int, $perPage: Int, $seasonYear: Int!) {
                Page (page: $page, perPage: $perPage) {
                  pageInfo {
                  total
                  currentPage
                  lastPage
                  hasNextPage
                  perPage
                  }
                media(id: $id, season: FALL, seasonYear: $seasonYear type: ANIME isAdult: false) {
                  id
                  title {
                    romaji
                    english
                  }
                  coverImage {
                  extraLarge
                  }
                  description
                  seasonYear
                  episodes
                  genres
                }
              }
              }
            `,
                    variables: {
                        season: args.season,
                        seasonYear: args.seasonYear,
                        page: args.page,
                        perPage: args.perPage,
                    },
                };
                const response = await axios({
                    url: endpoint,
                    method: "POST",
                    headers: headers,
                    data: graphqlQuery,
                });
                let animeArray = []

                const anime = response.data.data.Page.media
                // For Loop to convert Anilist Naming to My Naming System.
                for (let i = 0; i < anime.length; ++i) {

                    let titleName = ''

                    if (anime[i].title.english == undefined) {
                        titleName = anime[i].title.romaji
                    } else {
                        titleName = anime[i].title.english
                    }

                    const ani = {
                        id: anime[i].id,
                        title: titleName,
                        anilistId: anime[i].id,
                        image: anime[i].coverImage.extraLarge,
                        description: anime[i].description,
                        release: anime[i].seasonYear,
                        episodes: anime[i].episodes,
                        genres: anime[i].genres
                    }
                    animeArray.push(ani)
                }

                const fetchAnime = {
                    total: response.data.data.Page.pageInfo.total,
                    currentPage: response.data.data.Page.pageInfo.currentPage,
                    lastPage: response.data.data.Page.pageInfo.lastPage,
                    hasNextPage: response.data.data.Page.pageInfo.hasNextPage,
                    perPage: response.data.data.Page.pageInfo.perPage,
                    animes: animeArray
                };

                return fetchAnime;
            }
        },
    },

}