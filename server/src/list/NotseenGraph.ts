import NotSeen from '../models/notseen'
import NotGood from '../models/notgood'
import Favorite from '../models/favorite'
import User from '../models/user'
import { verifyToken } from '../config/authenicate'
import { ListTypes } from '../types'

interface NotSeenFace {
  id: string,
  anime: string
  token: string
  list: string
}

export const typesDef = `
    type Query {
        notseen(token: String!): [NotSeen]
    }

    type NotSeen {
        id: ID
        user: String
        anime: Anime
        rating: Int
        ErrorMessage: String
    }

    type Mutation {
        addNotSeen(anime: String! token: String! list: String!): Favorite
        removeNotSeen(id: String!): Favorite
    }
`

export const resolver = {
  Query: {
    notseen: async (_: unknown, args: ListTypes) => {
      const vtoken = verifyToken(args.token)
      const notseen = await NotSeen.find({ user: vtoken.id }).populate(
        "anime"
      );
      if (notseen) {
        return notseen;
      } else {
        return { id: '0' }
      }
    },
  },
  Mutation: {
    // Adds the Anime to the User Favorite List
    addNotSeen: async (_: unknown, args: NotSeenFace) => {
      try {
        const vtoken = verifyToken(args.token)
        // Checks to see if the token is valid
        if(vtoken.id === "error") return { ErrorMessage: "Your Token is not Valid. Please Log Back In" }
        let checkToken: boolean = false
        // Get User Information
        const users = await User.findOne({ _id: vtoken.id })
        // Checks to see if the user Tokens
        if (users?.tokens) {
          // Loops through the Tokens
          for (let x = 0; x < users.tokens.length; x++) {
            // Check To see if the user token is in the token list
            if (users.tokens[x].token === args.token) {
              // If Token is Found. If changes CheckToken to True
              checkToken = true
            }
          }
        }
        if (checkToken) {
          if (args.list == "favorite") { await Favorite.deleteMany({ user: vtoken.id, anime: args.anime }) };
          if (args.list == "notgood") { await NotGood.deleteMany({ user: vtoken.id, anime: args.anime }) };
          if (args.list == "notseen") { await NotSeen.deleteMany({ user: vtoken.id, anime: args.anime }) };
          let response = await NotSeen.create({
            user: vtoken.id,
            anime: args.anime,
          });
          return response;
        } else {
          return { ErrorMessage: "Token Error so Please Log Back In" }
        }
      } catch (e) {
        console.log(e);
      }
    },
    // Removes the Anime from the User Favorite List
    removeNotSeen: async (_: unknown, args: NotSeenFace) => {
      try {
        const vtoken = verifyToken(args.token)
        let checkToken: boolean = false
        // Get User Information
        const users = await User.findOne({ _id: vtoken.id })
        // Checks to see if the user Tokens
        if (users?.tokens) {
          // Loops through the Tokens
          for (let x = 0; x < users.tokens.length; x++) {
            // Check To see if the user token is in the token list
            if (users.tokens[x].token === args.token) {
              // If Token is Found. If changes CheckToken to True
              checkToken = true
            }
          }
        }
        if (checkToken) {
          await NotSeen.deleteOne({ _id: args.id }).populate("anime");
          return { message: "The Anime was Delete From your Favorite List" };
        } else {
          return { ErrorMessage: "Token Error so Please Log Back In"}
        }
      } catch (e) {
        console.log(e);
      }
    }
  }
}
