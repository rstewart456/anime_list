import Favorite from '../models/favorite'
import NotGood from '../models/notgood'
import NotSeen from '../models/notseen'
import User from '../models/user'
import { verifyToken } from '../config/authenicate'
import { ListTypes } from '../types'

interface FavoriteFace {
  id: string,
  anime: string
  token: string
  list: string
}

export const typesDef = `
    type Query {
        favorite(token: String!): [Favorite]
    }

    type Favorite {
        id: ID
        user: String
        anime: Anime
        rating: Int
        ErrorMessage: String
    }

    type Mutation {
        addFavorite(anime: String! token: String! list: String!): Favorite
        removeFavorite(id: String!): Favorite
    }
`

export const resolver = {
  Query: {
    favorite: async (_: unknown, args: ListTypes) => {
      const vtoken = verifyToken(args.token)
      const favorite = await Favorite.find({ user: vtoken.id }).populate(
        "anime"
      );
      if (favorite) {
        return favorite;
      } else {
        return { id: "0" }
      }
    },
  },
  Mutation: {
    // Adds the Anime to the User Favorite List
    addFavorite: async (_: unknown, args: FavoriteFace) => {
      try {
        const vtoken = verifyToken(args.token)
        let checkToken: boolean = false
         // Checks to see if the token is valid
        if(vtoken.id === "error") return { ErrorMessage: "Token is invalid. Please Log Back In" }
        // Get User Information
        const users = await User.findOne({ _id: vtoken.id })
        // Checks to see if the user Tokens
        if (users?.tokens) {
          // Loops through the Tokens
          for (let x = 0; x < users.tokens.length; x++) {
            // Check To see if the user token is in the token list
            if (users.tokens[x].token === args.token) {
              // If Token is Found. If changes CheckToken to True
              checkToken = true
            }
          }
        }
        if (checkToken) {
          if (args.list == "favorite") { await Favorite.deleteMany({ user: vtoken.id, anime: args.anime }) };
          if (args.list == "notgood") { await NotGood.deleteMany({ user: vtoken.id, anime: args.anime }) };
          if (args.list == "notseen") { await NotSeen.deleteMany({ user: vtoken.id, anime: args.anime }) };
          let response = await Favorite.create({
            user: vtoken.id,
            anime: args.anime,
          });
          return response;
        } else {
          return { ErrorMessage: "You are not Authorizte so Please Log Back In" }
        }
      } catch (e) {
        console.log(e);
      }
    },
    // Removes the Anime from the User Favorite List
    removeFavorite: async (_: unknown, args: FavoriteFace) => {
      try {
        const vtoken = verifyToken(args.token)
        // Checks to see if the token is valid
        if(vtoken.id == "error") return { ErrorMessage: "Token is invalid. Please Log Back In" }
        let checkToken: boolean = false
        // Get User Information
        const users = await User.findOne({ _id: vtoken.id })
        // Checks to see if the user Tokens
        if (users?.tokens) {
          // Loops through the Tokens
          for (let x = 0; x < users.tokens.length; x++) {
            // Check To see if the user token is in the token list
            if (users.tokens[x].token === args.token) {
              // If Token is Found. If changes CheckToken to True
              checkToken = true
            }
          }
        }
        if (checkToken) {
          await Favorite.deleteOne({ _id: args.id }).populate("anime");
          return { message: "The Anime was Delete From your Favorite List" };
        } else {
          return { ErrorMessage: "Please Log Back In"}
        }
      } catch (e) {
        console.log(e);
      }
    }
  }
}
