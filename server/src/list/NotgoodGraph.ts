import NotGood from '../models/notgood'
import Favorite from '../models/favorite'
import NotSeen from '../models/notseen'
import User from '../models/user'
import { verifyToken } from '../config/authenicate'
import { ListTypes } from '../types'

interface NotGoodFace {
  id: string,
  anime: string
  token: string
  list: string
}

export const typesDef = `
    type Query {
        notgood(token: String!): [NotGood]
    }

    type NotGood{
        id: ID
        user: String
        anime: Anime
        rating: Int
        ErrorMessage: String
    }

    type Mutation {
        addNotGood(anime: String! token: String! list: String!): NotGood
        removeNotGood(id: String!): NotGood
    }
`

export const resolver = {
  Query: {
    notgood: async (_: unknown, args: ListTypes) => {
      const vtoken = verifyToken(args.token)
      const notgood = await NotGood.find({ user: vtoken.id }).populate(
        "anime"
      );
      if (notgood) {
        return notgood;
      } else {
        return { id: '0' }
      }
    },
  },
  Mutation: {
    // Adds the Anime to the User Favorite List
    addNotGood: async (_: unknown, args: NotGoodFace) => {
      try {
        const vtoken = verifyToken(args.token)
        let checkToken: boolean = false
        // Checks to see if the token is valid
        if(vtoken.id == "error") return { ErrorMessage: "Token is invalid. Please Log Back In" }
        // Get User Information
        const users = await User.findOne({ _id: vtoken.id })
        // Checks to see if the user Tokens
        if (users?.tokens) {
          // Loops through the Tokens
          for (let x = 0; x < users.tokens.length; x++) {
            // Check To see if the user token is in the token list
            if (users.tokens[x].token === args.token) {
              // If Token is Found. If changes CheckToken to True
              checkToken = true
            }
          }
        }
        if (checkToken) {
          if (args.list == "favorite") { await Favorite.deleteMany({ user: vtoken.id, anime: args.anime }) };
          if (args.list == "notgood") { await NotGood.deleteMany({ user: vtoken.id, anime: args.anime }) };
          if (args.list == "notseen") { await NotSeen.deleteMany({ user: vtoken.id, anime: args.anime }) };
          let response = await NotGood.create({
            user: vtoken.id,
            anime: args.anime,
          });
          return response;
        } else {
          return { ErrorMessage: "Token error so Please Log Back In"}
        }
      } catch (e) {
        console.log(e);
      }
    },
    // Removes the Anime from the User Favorite List
    removeNotGood: async (_: unknown, args: NotGoodFace) => {
      try {
        const vtoken = verifyToken(args.token)
        // Checks to see if the token is valid
        if(vtoken.id === "error") return { ErrorMessage: "Token is invalid. Please Log Back In" }
        let checkToken: boolean = false
        // Get User Information
        const users = await User.findOne({ _id: vtoken.id })
        // Checks to see if the user Tokens
        if (users?.tokens) {
          // Loops through the Tokens
          for (let x = 0; x < users.tokens.length; x++) {
            // Check To see if the user token is in the token list
            if (users.tokens[x].token === args.token) {
              // If Token is Found. If changes CheckToken to True
              checkToken = true
            }
          }
        }
        if (checkToken) {
          await NotGood.deleteOne({ _id: args.id }).populate("anime");
          return { message: "The Anime was Delete From your Favorite List" };
        } else {
          return { ErrorMessage: "Token Error Please Log Back End"}
        }
      } catch (e) {
        console.log(e);
      }
    }
  }
}
