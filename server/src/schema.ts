import { makeExecutableSchema } from '@graphql-tools/schema'
import { typesDef as userDef, resolver as userResolver } from './user/UserGraph'
import { typesDef as animeDefs, resolver as animeResolver} from './anime/AnimeGraph'
import { typesDef as searchDefs, resolver as searchResolver } from './anime/SearchGraph'
import { typesDef as seasonDefs, resolver as SeasonResolver } from './anime/SeasonGraph'
import { typesDef as favoriteDefs, resolver as favoriteResolver } from './list/FavoriteGraph'
import { typesDef as notseenDefs, resolver as notseenResolver } from './list/NotseenGraph'
import { typesDef as notgoodDefs, resolver as notgoodResolver } from './list/NotgoodGraph'
import { typesDef as getseasonDefs, resolver as getseasonRosolver } from './anime/NextSeason'

export const schema = makeExecutableSchema({
  resolvers: 
  [ userResolver,
    animeResolver,
    searchResolver,
    favoriteResolver,
    notseenResolver,
    notgoodResolver,
    SeasonResolver,
    getseasonRosolver,
  ],
  typeDefs: 
  [
    userDef, 
    animeDefs,
    searchDefs,
    favoriteDefs,
    notseenDefs,
    notgoodDefs,
    seasonDefs,
    getseasonDefs
  ]
})
