import { Types } from 'mongoose'

export interface IUser {
    id?: Types.ObjectId,
    username: string,
    email: string,
    password: string,
    admin: boolean,
    token: string
  }

  export interface Animes {
    id: Types.ObjectId,
    anilistId: number,
    title: string,
    image: string,
    description: string,
    genres: Types.Array<string>,
    episodes: number,
    release: number,
    token: string
  }

  export interface Search extends Animes {
    search: string,
    page: number
    perPage: number
  }

  export interface Season extends Animes {
    season: string,
    seasonYear: number,
    page: number,
    perPage: number
  }

  export interface ExtendUser extends IUser {
    _id: string
  }

  export interface ListTypes {
      id: string,
      user: string,
      anime: Animes
      token: string,
  }
