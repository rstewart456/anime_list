import User from '../models/user'
import { verifyToken, accessToken } from '../config/authenicate'
import { IUser } from '../types'
import bcrypt from 'bcrypt'

export const typesDef = `
  type Query {
    me(token: String!): User
    users: [User]
  }

  type Mutation {
    addUser(username: String!, email: String!, password: String!): User
    logIn(email: String!, password: String!): User
  }
  
  type User {
    id: ID
    username: String
    email: String
    admin: Boolean
    token: String
    ErrorMessage: String
  }

`
interface Hello extends IUser {
  _id: string
}

export const resolver = {
  Query: {
    users: async () => {
      const user = await User.find()
      if (!user) {
        return null
      } else {
        return user
      }
    },
    me: async function (_: unknown, args: IUser) {
      const token = verifyToken(args.token)
      let checkToken: boolean = false
      //const users = await User.findOne({ tokens: { $elemMatch: { token: args.token }}})
      if(token.id == "error") return { ErrorMessage: "The Token is not Valid Please Log Back End"}
      const users = await User.findOne({ _id: token.id })
      if(!token) return { ErrorMessage: "Your Token is not Valid Please Log Back In"}
      if(!users) return { ErrorMessage: "User is Not Found" }
      if(users.tokens.length < 1) return { ErrorMessage: "No Tokens in Databse so Please Log back In" }
      for (let x = 0; x < users.tokens.length; x++) {
        if(users.tokens[x].token === args.token) {
          checkToken = true
        }
      }
      if(!checkToken) return { ErrorMessage: "Your Token in not in DataBase so Please Log Back In"}
      return users
      //if (users?.tokens) {
      //  for (let x = 0; x < users.tokens.length; x++) {
      //    if (users.tokens[x].token === args.token) {
      //      checkToken = true
      //    }
      //  }
      //} else {
      //  return { ErrorMessage: "Please Log Back In" }
      //}
      //if (token && checkToken) {
      //  const user = await User.findById(token.id)
      //  return user
      //}
    }
  },
  Mutation: {
    addUser: async (_: unknown, args: IUser) => {
      try {
        const check = await User.findOne({ email: args.email })
        if (check) {
          return { ErrorMessage: "The Email is already in the DataBase" }
        } else {
          const hashPassword = await bcrypt.hash(args.password, 10)
          const useradd = {
            username: args.username,
            email: args.email,
            password: hashPassword,
          }
          await User.create(useradd)
          const user = await User.findOne({ email: args.email }) as Hello
          const geToken = accessToken(user._id)
          const getUser = {
            id: user._id,
            username: user.username,
            email: user.email,
            token: geToken
          }
          return getUser
        }
      } catch (e) {
        console.log(e)
      }
    },
    logIn: async (_: unknown, args: IUser) => {
      try {
        // Gives you the Date from 30 Days Ago.
        const thirtyDays = new Date(Date.now() - 60 * 60 * 24 * 30 * 1000)
        //const tenMinutes = new Date(Date.now() - 10 * 60 * 1000)
        const user = await User.findOne({ email: args.email }) as Hello
        if(!user) return { ErrorMessage: "No User is Found" }
        const compare = await bcrypt.compare(args.password, user.password)
        const geToken = accessToken(user._id)
          if (compare) {
          // Stores the new Token in the DataBase
          await User.findOneAndUpdate({ email: args.email }, { $push: { tokens: { token: geToken } } })
          // Deletes any Tokens that is over Thirty Days old
          await User.findOneAndUpdate({ email: args.email }, { $pull: { tokens: { createdAt: { $lte: thirtyDays } } } })
          // Get user Information to be sent to the Client
          const getuser = {
            id: user._id,
            username: user.username,
            email: user.email,
            token: geToken
          }
          return getuser
        } else {
          return { ErrorMessage: "Wrong Password" }
        }
      } catch (e) {
        console.log(e)
      }
    }
  }
}
