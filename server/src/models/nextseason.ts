import mongoose from 'mongoose';
import { mongoosePagination, Pagination } from 'mongoose-paginate-ts'

const Schema = mongoose.Schema;

type NextSeasonType = mongoose.Document & {
  anilist: Number,
  title: String,
  image: String,
  description: String,
  genres: [String],
  release: String,
  eposites: Number
}

const nextseasonSchema = new Schema ({
  anilistId: {
    type: Number,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  image: {
    type: String,
    default: '/images/anime.png'
  },
  description: {
    type: String
  },
  genres: {
    type: [String]
  },
  release: {
    type: Number
  },
  episodes: {
    type: Number
  }
})

nextseasonSchema.plugin(mongoosePagination)

export const NextSeason: Pagination<NextSeasonType> = mongoose.model<NextSeasonType, Pagination<NextSeasonType>>("NextSeason", nextseasonSchema)
//export default mongoose.model('NextSeason', nextseasonSchema)
