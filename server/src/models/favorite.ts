import { Schema, model} from 'mongoose'

const favoriteSchema = new Schema ({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  anime: {
    type: Schema.Types.ObjectId,
    ref: 'Anime'
  },
  rating: {
    type: Number,
    default: 5
  },
}, {timestamps: true})

export default model('Favorite', favoriteSchema)
