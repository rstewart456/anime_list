import { Schema, model } from 'mongoose'

const Tokens = new Schema ({
  token: {
    type: String,
  }
}, {timestamps: true})

const userSchema = new Schema ({
  username: {
    type: String,
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  admin: {
    type: Boolean,
    default: false
  },
  tokens: {
    type: [Tokens]
  }
})

//Tokens.index({ createdAt: 1 }, { expireAfterSeconds: 2592000 })

export default model('User', userSchema)
