import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const animeSchema = new Schema ({
  anilistId: {
    type: Number,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  image: {
    type: String,
    default: '/images/anime.png'
  },
  description: {
    type: String
  },
  genres: {
    type: [String]
  },
  release: {
    type: Number
  },
  episodes: {
    type: Number
  }
})

export default mongoose.model('Anime', animeSchema)
