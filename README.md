# The Anime List App

The has the Front-end and the Back-end. I decided to have one respository for the front and back end. I did method so I use Docker to create and host the App.

# Back end 
Technology
- Graphql
- Graphql-yoga
- Typescript
- Mongoose

# Front end
Technology
- Svelte
- Tailwindcss
- Typescript
- Vite
- Codegen
- Apollo Client