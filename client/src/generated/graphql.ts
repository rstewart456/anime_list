import client from "/src/client.js";
import type {
        ApolloQueryResult, ObservableQuery, WatchQueryOptions, QueryOptions, MutationOptions
      } from "@apollo/client";
import { readable } from "svelte/store";
import type { Readable } from "svelte/store";
import gql from "graphql-tag"
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Anime = {
  __typename?: 'Anime';
  ErrorMessage?: Maybe<Scalars['String']>;
  anilistId: Scalars['Int'];
  description?: Maybe<Scalars['String']>;
  episodes?: Maybe<Scalars['Int']>;
  genres?: Maybe<Array<Maybe<Scalars['String']>>>;
  id: Scalars['ID'];
  image: Scalars['String'];
  release?: Maybe<Scalars['Int']>;
  title: Scalars['String'];
  token: Scalars['String'];
};

export type Favorite = {
  __typename?: 'Favorite';
  ErrorMessage?: Maybe<Scalars['String']>;
  anime?: Maybe<Anime>;
  id?: Maybe<Scalars['ID']>;
  rating?: Maybe<Scalars['Int']>;
  user?: Maybe<Scalars['String']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  addAnime?: Maybe<Anime>;
  addFavorite?: Maybe<Favorite>;
  addNotGood?: Maybe<NotGood>;
  addNotSeen?: Maybe<Favorite>;
  addUser?: Maybe<User>;
  getseason?: Maybe<Array<Maybe<Anime>>>;
  logIn?: Maybe<User>;
  removeFavorite?: Maybe<Favorite>;
  removeNotGood?: Maybe<NotGood>;
  removeNotSeen?: Maybe<Favorite>;
};


export type MutationAddAnimeArgs = {
  anilistId: Scalars['Int'];
  description: Scalars['String'];
  episodes?: InputMaybe<Scalars['Int']>;
  genres?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  image: Scalars['String'];
  release?: InputMaybe<Scalars['Int']>;
  title: Scalars['String'];
  token: Scalars['String'];
};


export type MutationAddFavoriteArgs = {
  anime: Scalars['String'];
  list: Scalars['String'];
  token: Scalars['String'];
};


export type MutationAddNotGoodArgs = {
  anime: Scalars['String'];
  list: Scalars['String'];
  token: Scalars['String'];
};


export type MutationAddNotSeenArgs = {
  anime: Scalars['String'];
  list: Scalars['String'];
  token: Scalars['String'];
};


export type MutationAddUserArgs = {
  email: Scalars['String'];
  password: Scalars['String'];
  username: Scalars['String'];
};


export type MutationLogInArgs = {
  email: Scalars['String'];
  password: Scalars['String'];
};


export type MutationRemoveFavoriteArgs = {
  id: Scalars['String'];
};


export type MutationRemoveNotGoodArgs = {
  id: Scalars['String'];
};


export type MutationRemoveNotSeenArgs = {
  id: Scalars['String'];
};

export type NotGood = {
  __typename?: 'NotGood';
  ErrorMessage?: Maybe<Scalars['String']>;
  anime?: Maybe<Anime>;
  id?: Maybe<Scalars['ID']>;
  rating?: Maybe<Scalars['Int']>;
  user?: Maybe<Scalars['String']>;
};

export type NotSeen = {
  __typename?: 'NotSeen';
  ErrorMessage?: Maybe<Scalars['String']>;
  anime?: Maybe<Anime>;
  id?: Maybe<Scalars['ID']>;
  rating?: Maybe<Scalars['Int']>;
  user?: Maybe<Scalars['String']>;
};

export type Pagination = {
  __typename?: 'Pagination';
  docs?: Maybe<Array<Maybe<Anime>>>;
  hasMore?: Maybe<Scalars['Boolean']>;
  limit?: Maybe<Scalars['Int']>;
  page?: Maybe<Scalars['Int']>;
  totalPages?: Maybe<Scalars['Int']>;
};

export type Query = {
  __typename?: 'Query';
  anime?: Maybe<Anime>;
  animes?: Maybe<Array<Maybe<Anime>>>;
  favorite?: Maybe<Array<Maybe<Favorite>>>;
  me?: Maybe<User>;
  notgood?: Maybe<Array<Maybe<NotGood>>>;
  notseen?: Maybe<Array<Maybe<NotSeen>>>;
  searches?: Maybe<Search>;
  season?: Maybe<Season>;
  showseason?: Maybe<Pagination>;
  users?: Maybe<Array<Maybe<User>>>;
};


export type QueryAnimeArgs = {
  anilistId: Scalars['Int'];
};


export type QueryFavoriteArgs = {
  token: Scalars['String'];
};


export type QueryMeArgs = {
  token: Scalars['String'];
};


export type QueryNotgoodArgs = {
  token: Scalars['String'];
};


export type QueryNotseenArgs = {
  token: Scalars['String'];
};


export type QuerySearchesArgs = {
  page?: InputMaybe<Scalars['Int']>;
  perPage?: InputMaybe<Scalars['Int']>;
  search: Scalars['String'];
};


export type QuerySeasonArgs = {
  page?: InputMaybe<Scalars['Int']>;
  perPage?: InputMaybe<Scalars['Int']>;
  season?: InputMaybe<Scalars['String']>;
  seasonYear?: InputMaybe<Scalars['Int']>;
};


export type QueryShowseasonArgs = {
  limit: Scalars['Int'];
  page: Scalars['Int'];
};

export type Search = {
  __typename?: 'Search';
  animes?: Maybe<Array<Maybe<Anime>>>;
  currentPage?: Maybe<Scalars['Int']>;
  hasNextPage?: Maybe<Scalars['Boolean']>;
  id: Scalars['ID'];
  lastPage?: Maybe<Scalars['Int']>;
  perPage?: Maybe<Scalars['Int']>;
  search: Scalars['String'];
  total?: Maybe<Scalars['Int']>;
};

export type Season = {
  __typename?: 'Season';
  animes?: Maybe<Array<Maybe<Anime>>>;
  currentPage?: Maybe<Scalars['Int']>;
  hasNextPage?: Maybe<Scalars['Boolean']>;
  id: Scalars['ID'];
  lastPage?: Maybe<Scalars['Int']>;
  perPage?: Maybe<Scalars['Int']>;
  season?: Maybe<Scalars['String']>;
  seasonYear: Scalars['Int'];
  total?: Maybe<Scalars['Int']>;
};

export type User = {
  __typename?: 'User';
  ErrorMessage?: Maybe<Scalars['String']>;
  admin?: Maybe<Scalars['Boolean']>;
  email?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['ID']>;
  token?: Maybe<Scalars['String']>;
  username?: Maybe<Scalars['String']>;
};

export type AddFavoriteMutationVariables = Exact<{
  anime: Scalars['String'];
  token: Scalars['String'];
  list: Scalars['String'];
}>;


export type AddFavoriteMutation = { __typename?: 'Mutation', addFavorite?: { __typename?: 'Favorite', id?: string | null, ErrorMessage?: string | null } | null };

export type AddNotGoodMutationVariables = Exact<{
  anime: Scalars['String'];
  token: Scalars['String'];
  list: Scalars['String'];
}>;


export type AddNotGoodMutation = { __typename?: 'Mutation', addNotGood?: { __typename?: 'NotGood', id?: string | null, ErrorMessage?: string | null } | null };

export type AddNotSeenMutationVariables = Exact<{
  anime: Scalars['String'];
  token: Scalars['String'];
  list: Scalars['String'];
}>;


export type AddNotSeenMutation = { __typename?: 'Mutation', addNotSeen?: { __typename?: 'Favorite', id?: string | null, ErrorMessage?: string | null } | null };

export type AddAnimeMutationVariables = Exact<{
  anilistId: Scalars['Int'];
  title: Scalars['String'];
  image: Scalars['String'];
  description: Scalars['String'];
  genres?: InputMaybe<Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>>;
  release?: InputMaybe<Scalars['Int']>;
  episodes?: InputMaybe<Scalars['Int']>;
  token: Scalars['String'];
}>;


export type AddAnimeMutation = { __typename?: 'Mutation', addAnime?: { __typename?: 'Anime', id: string, ErrorMessage?: string | null } | null };

export type LogInMutationVariables = Exact<{
  email: Scalars['String'];
  password: Scalars['String'];
}>;


export type LogInMutation = { __typename?: 'Mutation', logIn?: { __typename?: 'User', token?: string | null, ErrorMessage?: string | null } | null };

export type AddUserMutationVariables = Exact<{
  username: Scalars['String'];
  email: Scalars['String'];
  password: Scalars['String'];
}>;


export type AddUserMutation = { __typename?: 'Mutation', addUser?: { __typename?: 'User', token?: string | null, ErrorMessage?: string | null } | null };

export type AnimeInfoQueryVariables = Exact<{
  anilistId: Scalars['Int'];
}>;


export type AnimeInfoQuery = { __typename?: 'Query', anime?: { __typename?: 'Anime', id: string, anilistId: number, title: string, image: string, description?: string | null, genres?: Array<string | null> | null, release?: number | null, episodes?: number | null, ErrorMessage?: string | null } | null };

export type ListQueryVariables = Exact<{
  token: Scalars['String'];
}>;


export type ListQuery = { __typename?: 'Query', favorite?: Array<{ __typename?: 'Favorite', id?: string | null, rating?: number | null, ErrorMessage?: string | null, anime?: { __typename?: 'Anime', id: string, anilistId: number, title: string, image: string } | null } | null> | null, notseen?: Array<{ __typename?: 'NotSeen', id?: string | null, rating?: number | null, ErrorMessage?: string | null, anime?: { __typename?: 'Anime', id: string, anilistId: number, title: string, image: string } | null } | null> | null, notgood?: Array<{ __typename?: 'NotGood', id?: string | null, rating?: number | null, ErrorMessage?: string | null, anime?: { __typename?: 'Anime', id: string, anilistId: number, title: string, image: string } | null } | null> | null };

export type NextanimeseasonQueryVariables = Exact<{
  page: Scalars['Int'];
  limit: Scalars['Int'];
}>;


export type NextanimeseasonQuery = { __typename?: 'Query', showseason?: { __typename?: 'Pagination', totalPages?: number | null, page?: number | null, limit?: number | null, hasMore?: boolean | null, docs?: Array<{ __typename?: 'Anime', anilistId: number, title: string, image: string, description?: string | null, genres?: Array<string | null> | null, release?: number | null, episodes?: number | null, ErrorMessage?: string | null } | null> | null } | null };

export type SearchQueryVariables = Exact<{
  search: Scalars['String'];
  page?: InputMaybe<Scalars['Int']>;
  perPage?: InputMaybe<Scalars['Int']>;
}>;


export type SearchQuery = { __typename?: 'Query', searches?: { __typename?: 'Search', total?: number | null, hasNextPage?: boolean | null, lastPage?: number | null, animes?: Array<{ __typename?: 'Anime', anilistId: number, title: string, image: string, description?: string | null, release?: number | null, genres?: Array<string | null> | null, episodes?: number | null } | null> | null } | null };

export type SeasonQueryVariables = Exact<{
  season: Scalars['String'];
  seasonYear: Scalars['Int'];
  page?: InputMaybe<Scalars['Int']>;
  perPage?: InputMaybe<Scalars['Int']>;
}>;


export type SeasonQuery = { __typename?: 'Query', season?: { __typename?: 'Season', total?: number | null, hasNextPage?: boolean | null, lastPage?: number | null, animes?: Array<{ __typename?: 'Anime', anilistId: number, title: string, image: string, description?: string | null, release?: number | null, genres?: Array<string | null> | null, episodes?: number | null } | null> | null } | null };

export type MeQueryVariables = Exact<{
  token: Scalars['String'];
}>;


export type MeQuery = { __typename?: 'Query', me?: { __typename?: 'User', id?: string | null, username?: string | null, ErrorMessage?: string | null } | null };


export const AddFavoriteDoc = gql`
    mutation AddFavorite($anime: String!, $token: String!, $list: String!) {
  addFavorite(anime: $anime, token: $token, list: $list) {
    id
    ErrorMessage
  }
}
    `;
export const AddNotGoodDoc = gql`
    mutation AddNotGood($anime: String!, $token: String!, $list: String!) {
  addNotGood(anime: $anime, token: $token, list: $list) {
    id
    ErrorMessage
  }
}
    `;
export const AddNotSeenDoc = gql`
    mutation AddNotSeen($anime: String!, $token: String!, $list: String!) {
  addNotSeen(anime: $anime, token: $token, list: $list) {
    id
    ErrorMessage
  }
}
    `;
export const AddAnimeDoc = gql`
    mutation AddAnime($anilistId: Int!, $title: String!, $image: String!, $description: String!, $genres: [String], $release: Int, $episodes: Int, $token: String!) {
  addAnime(
    anilistId: $anilistId
    title: $title
    image: $image
    description: $description
    genres: $genres
    release: $release
    episodes: $episodes
    token: $token
  ) {
    id
    ErrorMessage
  }
}
    `;
export const LogInDoc = gql`
    mutation LogIn($email: String!, $password: String!) {
  logIn(email: $email, password: $password) {
    token
    ErrorMessage
  }
}
    `;
export const AddUserDoc = gql`
    mutation AddUser($username: String!, $email: String!, $password: String!) {
  addUser(username: $username, email: $email, password: $password) {
    token
    ErrorMessage
  }
}
    `;
export const AnimeInfoDoc = gql`
    query animeInfo($anilistId: Int!) {
  anime(anilistId: $anilistId) {
    id
    anilistId
    title
    image
    description
    genres
    release
    episodes
    ErrorMessage
  }
}
    `;
export const ListDoc = gql`
    query List($token: String!) {
  favorite(token: $token) {
    id
    rating
    ErrorMessage
    anime {
      id
      anilistId
      title
      image
    }
  }
  notseen(token: $token) {
    id
    rating
    ErrorMessage
    anime {
      id
      anilistId
      title
      image
    }
  }
  notgood(token: $token) {
    id
    rating
    ErrorMessage
    anime {
      id
      anilistId
      title
      image
    }
  }
}
    `;
export const NextanimeseasonDoc = gql`
    query nextanimeseason($page: Int!, $limit: Int!) {
  showseason(page: $page, limit: $limit) {
    totalPages
    page
    limit
    hasMore
    docs {
      anilistId
      title
      image
      description
      genres
      release
      episodes
      ErrorMessage
    }
  }
}
    `;
export const SearchDoc = gql`
    query Search($search: String!, $page: Int, $perPage: Int) {
  searches(search: $search, page: $page, perPage: $perPage) {
    total
    hasNextPage
    lastPage
    animes {
      anilistId
      title
      image
      description
      release
      genres
      episodes
    }
  }
}
    `;
export const SeasonDoc = gql`
    query Season($season: String!, $seasonYear: Int!, $page: Int, $perPage: Int) {
  season(season: $season, seasonYear: $seasonYear, page: $page, perPage: $perPage) {
    total
    hasNextPage
    lastPage
    animes {
      anilistId
      title
      image
      description
      release
      genres
      episodes
    }
  }
}
    `;
export const MeDoc = gql`
    query Me($token: String!) {
  me(token: $token) {
    id
    username
    ErrorMessage
  }
}
    `;
export const AddFavorite = (
            options: Omit<
              MutationOptions<any, AddFavoriteMutationVariables>, 
              "mutation"
            >
          ) => {
            const m = client.mutate<AddFavoriteMutation, AddFavoriteMutationVariables>({
              mutation: AddFavoriteDoc,
              ...options,
            });
            return m;
          }
export const AddNotGood = (
            options: Omit<
              MutationOptions<any, AddNotGoodMutationVariables>, 
              "mutation"
            >
          ) => {
            const m = client.mutate<AddNotGoodMutation, AddNotGoodMutationVariables>({
              mutation: AddNotGoodDoc,
              ...options,
            });
            return m;
          }
export const AddNotSeen = (
            options: Omit<
              MutationOptions<any, AddNotSeenMutationVariables>, 
              "mutation"
            >
          ) => {
            const m = client.mutate<AddNotSeenMutation, AddNotSeenMutationVariables>({
              mutation: AddNotSeenDoc,
              ...options,
            });
            return m;
          }
export const AddAnime = (
            options: Omit<
              MutationOptions<any, AddAnimeMutationVariables>, 
              "mutation"
            >
          ) => {
            const m = client.mutate<AddAnimeMutation, AddAnimeMutationVariables>({
              mutation: AddAnimeDoc,
              ...options,
            });
            return m;
          }
export const LogIn = (
            options: Omit<
              MutationOptions<any, LogInMutationVariables>, 
              "mutation"
            >
          ) => {
            const m = client.mutate<LogInMutation, LogInMutationVariables>({
              mutation: LogInDoc,
              ...options,
            });
            return m;
          }
export const AddUser = (
            options: Omit<
              MutationOptions<any, AddUserMutationVariables>, 
              "mutation"
            >
          ) => {
            const m = client.mutate<AddUserMutation, AddUserMutationVariables>({
              mutation: AddUserDoc,
              ...options,
            });
            return m;
          }
export const animeInfo = (
            options: Omit<
              WatchQueryOptions<AnimeInfoQueryVariables>, 
              "query"
            >
          ): Readable<
            ApolloQueryResult<AnimeInfoQuery> & {
              query: ObservableQuery<
                AnimeInfoQuery,
                AnimeInfoQueryVariables
              >;
            }
          > => {
            const q = client.watchQuery({
              query: AnimeInfoDoc,
              ...options,
            });
            var result = readable<
              ApolloQueryResult<AnimeInfoQuery> & {
                query: ObservableQuery<
                  AnimeInfoQuery,
                  AnimeInfoQueryVariables
                >;
              }
            >(
              { data: {} as any, loading: true, error: undefined, networkStatus: 1, query: q },
              (set) => {
                q.subscribe((v: any) => {
                  set({ ...v, query: q });
                });
              }
            );
            return result;
          }
        
              export const AsyncanimeInfo = (
                options: Omit<
                  QueryOptions<AnimeInfoQueryVariables>,
                  "query"
                >
              ) => {
                return client.query<AnimeInfoQuery>({query: AnimeInfoDoc, ...options})
              }
            
export const List = (
            options: Omit<
              WatchQueryOptions<ListQueryVariables>, 
              "query"
            >
          ): Readable<
            ApolloQueryResult<ListQuery> & {
              query: ObservableQuery<
                ListQuery,
                ListQueryVariables
              >;
            }
          > => {
            const q = client.watchQuery({
              query: ListDoc,
              ...options,
            });
            var result = readable<
              ApolloQueryResult<ListQuery> & {
                query: ObservableQuery<
                  ListQuery,
                  ListQueryVariables
                >;
              }
            >(
              { data: {} as any, loading: true, error: undefined, networkStatus: 1, query: q },
              (set) => {
                q.subscribe((v: any) => {
                  set({ ...v, query: q });
                });
              }
            );
            return result;
          }
        
              export const AsyncList = (
                options: Omit<
                  QueryOptions<ListQueryVariables>,
                  "query"
                >
              ) => {
                return client.query<ListQuery>({query: ListDoc, ...options})
              }
            
export const nextanimeseason = (
            options: Omit<
              WatchQueryOptions<NextanimeseasonQueryVariables>, 
              "query"
            >
          ): Readable<
            ApolloQueryResult<NextanimeseasonQuery> & {
              query: ObservableQuery<
                NextanimeseasonQuery,
                NextanimeseasonQueryVariables
              >;
            }
          > => {
            const q = client.watchQuery({
              query: NextanimeseasonDoc,
              ...options,
            });
            var result = readable<
              ApolloQueryResult<NextanimeseasonQuery> & {
                query: ObservableQuery<
                  NextanimeseasonQuery,
                  NextanimeseasonQueryVariables
                >;
              }
            >(
              { data: {} as any, loading: true, error: undefined, networkStatus: 1, query: q },
              (set) => {
                q.subscribe((v: any) => {
                  set({ ...v, query: q });
                });
              }
            );
            return result;
          }
        
              export const Asyncnextanimeseason = (
                options: Omit<
                  QueryOptions<NextanimeseasonQueryVariables>,
                  "query"
                >
              ) => {
                return client.query<NextanimeseasonQuery>({query: NextanimeseasonDoc, ...options})
              }
            
export const Search = (
            options: Omit<
              WatchQueryOptions<SearchQueryVariables>, 
              "query"
            >
          ): Readable<
            ApolloQueryResult<SearchQuery> & {
              query: ObservableQuery<
                SearchQuery,
                SearchQueryVariables
              >;
            }
          > => {
            const q = client.watchQuery({
              query: SearchDoc,
              ...options,
            });
            var result = readable<
              ApolloQueryResult<SearchQuery> & {
                query: ObservableQuery<
                  SearchQuery,
                  SearchQueryVariables
                >;
              }
            >(
              { data: {} as any, loading: true, error: undefined, networkStatus: 1, query: q },
              (set) => {
                q.subscribe((v: any) => {
                  set({ ...v, query: q });
                });
              }
            );
            return result;
          }
        
              export const AsyncSearch = (
                options: Omit<
                  QueryOptions<SearchQueryVariables>,
                  "query"
                >
              ) => {
                return client.query<SearchQuery>({query: SearchDoc, ...options})
              }
            
export const Season = (
            options: Omit<
              WatchQueryOptions<SeasonQueryVariables>, 
              "query"
            >
          ): Readable<
            ApolloQueryResult<SeasonQuery> & {
              query: ObservableQuery<
                SeasonQuery,
                SeasonQueryVariables
              >;
            }
          > => {
            const q = client.watchQuery({
              query: SeasonDoc,
              ...options,
            });
            var result = readable<
              ApolloQueryResult<SeasonQuery> & {
                query: ObservableQuery<
                  SeasonQuery,
                  SeasonQueryVariables
                >;
              }
            >(
              { data: {} as any, loading: true, error: undefined, networkStatus: 1, query: q },
              (set) => {
                q.subscribe((v: any) => {
                  set({ ...v, query: q });
                });
              }
            );
            return result;
          }
        
              export const AsyncSeason = (
                options: Omit<
                  QueryOptions<SeasonQueryVariables>,
                  "query"
                >
              ) => {
                return client.query<SeasonQuery>({query: SeasonDoc, ...options})
              }
            
export const Me = (
            options: Omit<
              WatchQueryOptions<MeQueryVariables>, 
              "query"
            >
          ): Readable<
            ApolloQueryResult<MeQuery> & {
              query: ObservableQuery<
                MeQuery,
                MeQueryVariables
              >;
            }
          > => {
            const q = client.watchQuery({
              query: MeDoc,
              ...options,
            });
            var result = readable<
              ApolloQueryResult<MeQuery> & {
                query: ObservableQuery<
                  MeQuery,
                  MeQueryVariables
                >;
              }
            >(
              { data: {} as any, loading: true, error: undefined, networkStatus: 1, query: q },
              (set) => {
                q.subscribe((v: any) => {
                  set({ ...v, query: q });
                });
              }
            );
            return result;
          }
        
              export const AsyncMe = (
                options: Omit<
                  QueryOptions<MeQueryVariables>,
                  "query"
                >
              ) => {
                return client.query<MeQuery>({query: MeDoc, ...options})
              }
            