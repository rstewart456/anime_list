export type UserMe = {
  id: string
  username: string
}

export type List = {
  id?: string
  rating?: number
  anime?: {
    id?: string
    anilistId?: number
    title?: string
    image?: string
  }
}

export type Anime = {
  id?: string
  anilistId?: number
  title?: string
  image?: string
  description?: string
  genres?: string[]
  release?: number
  episodes?: number
}

export type Login = {
  email: string
  password: string
}

export type Validator = {
  username: boolean
  email: boolean
  password: boolean
  confirm: boolean
}

export interface Season extends Anime {
  season: string,
  seasonYear: number,
  page: number,
  perPage: number,
}