import { writable } from "svelte/store";
import type { User, SearchQuery } from './generated/graphql'
import type { List, Anime } from './types'

export const me = writable<User>({
  id: '',
  username: ''
})

export const Search = writable<SearchQuery>()

export const favorites = writable<List[]>([])

export const notseen = writable<List[]>([])

export const notgood = writable<List[]>([])

export const animeinfo = writable<Anime>()
 
export const list = writable<string>('')

