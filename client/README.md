# My Personal Anime List

I wanted to create an app that has three list. My Favorite, Not Seen and Not Good. This is the front-end to my app. This app has not associated with Anilist. You can type in you Search query and it search throuh the Anilist DataBase. After finding the anime. You can either added to Favorites, Not Seen, Not Good List.

## The Technology
- Svelte
- Tailwindcss
- Typescript
- Vite
- Codegen
- Apollo Client

I decided to use Svelte becouse of their global State Management System. It was one of the most easies one to setup and use.

# TODO'S
- [X] Log In Page
- [X] Home Page
- [X] Favorite Page
- [X] Not Seen Page
- [X] Not Good Page
- [X] Search for Anime from Anilist Database
- [X] Upate Anime Information. When You grab the information from Anilist Database.
- [X] Add Sign Up Page.
- [X] Access Token Error Handling.
- [ ] Server Error Handling.
- [ ] Fix Types 

# Future Request
- [ ] Add a profile page to update user information and to reset user device access.
- [ ] When Loading Grab the Up Coming Anime from the Anilist Database.
- [ ] When Loading Grub the Arring Now from the Anilist Database.
